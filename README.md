# itqn server deployment

1. To buy and start a server, please have a look at `terraform/`

2. The setup of the server is described in `ansible/`
   All services are configured and started in docker containers:

    - traefik as reverse proxy
    - wordpress for the www website
    - nextcloud for collaboration
    - mumble as audio chat platform
