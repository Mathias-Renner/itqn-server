
- ansible:
  - create ENV files using ansible, from a list of credentials/vars (in 'files') -> saved in KeePass (+ in .gitignore)
  - move restart-docker-handler further up after docker setup
  - backup and restore
  - test on domain
