# Use ansible to install docker and deploy containers with services

## How to setup

The `inventory` file needs to be created with this content:

```
server ansible_user=root ansible_host=<127.0.0.1>
```
***It gets also printed as result of the `terraform apply` step***


`docker-compose build`

`docker-compose run --rm ansible setup.yml`

(Optional) run specific role

`docker-compose run --rm ansible setup.yml -t <service>`

where service can be:
    - docker (install)
    - traefik
    - wordpress
    - nextcloud
    - mumble

***Note***

The role `setup_docker_environment` is a dependency for `run_service_*` roles
and does not need to be included in the playbook


## Upgrade the host packages with apt and reboot if needed and not blocked

The reboot can be blocked by creating a file `/root/block_reboot` on the server
if it is newer than 86400 seconds.

`docker-compose run --rm ansible upgrade.yml`

## Known issues

- Paramiko error 
     DDOS protection of Hetzner blocks many paralell SSH-connections.
