### Usage

1. Please copy <env.dist> to <.env> and modify it.
```
cat <<EOM | tee .env
# public domain
PUBLIC_DOMAIN=itqn.de
EOM
```

2. Create docker networks
```
docker network create net-proxy
```

3. Start infrastructure
```
docker-compose pull --ignore-pull-failures
docker-compose config -q # check for syntax errors
docker-compose up -d
```

This will start an
- traefik
- portainer

container.


4. Start / create services
```
cd services/<specific service>
cp ../../.env ./
docker-compose up -d
```
