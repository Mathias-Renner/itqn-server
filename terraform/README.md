
### Build hcloud terraform-provider-hcloud with:

[source](https://github.com/hetznercloud/terraform-provider-hcloud)

`docker-compose run --rm tfbuild`

### Set hcloud token
Edit `hetzner/credentials.tf` or use `-var=hcloud_token=...` in the cli

### Select image(ububtu-16.04 datacenter(fsn1-dc8) and server_type(cx11)
```
-var="hcloud_image=debian-9"
-var="hcloud_datacenter=nbg1-dc3"
-var="hcloud_server=cx21"
```

### SSH-keys
Put your ssh keys (`id_rsa.pub`) in `keys/`
and add them in `ssh_keys.tf`


### Use terraform

`docker-compose run --rm tf init`

`docker-compose run --rm tf fmt`

`docker-compose run --rm tf apply`

### Create the Server at Hetzner Cloud
`docker-compose run --rm tf apply -var="hcloud_token="`

### Remove the Server at Hetzner Cloud
`docker-compose run --rm tf destroy -var="hcloud_token="`

### Use of `terraform plan` to test the config

`docker-compose run --rm tf plan -out=itqn-server.plan`

`docker-compose run --rm tf apply -var="hcloud_token=" itqn-server.plan`
