# Set the variable value in *.tfvars file
# or using -var="hcloud_token=..." CLI option

variable "hcloud_token" {}

variable "hcloud_image" {
  default = "ubuntu-16.04"
}

variable "hcloud_datacenter" {
  default = "fsn1-dc8" # Falkenstein

  #default = "nbg1-dc3" # Nuremberg
}

variable "hcloud_server" {
  default = "cx11" # 2.96€ 1vCPU 2GBram 20GBssd

  #default = "cx21" # 5.83€ 2vCPU 4GBram 40GBssd  
  #default = "cx31" # 10.59€ 2vCPU 8GBram 80GBssd
  #default = "cx41" # 18.92€ 4vCPU 16GBram 160GBssd
}

output "ansible inventory" {
  value = "server ansible_user=root ansible_host=${hcloud_floating_ip.master6.ip_address}"
}
