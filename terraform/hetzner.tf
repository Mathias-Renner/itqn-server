# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

resource "hcloud_floating_ip" "master4" {
  type      = "ipv4"
  server_id = "${hcloud_server.node1.id}"
}

resource "hcloud_floating_ip" "master6" {
  type      = "ipv6"
  server_id = "${hcloud_server.node1.id}"
}

resource "hcloud_server" "node1" {
  name = "itqn"

  ssh_keys = ["${hcloud_ssh_key.default.id}",
    "${hcloud_ssh_key.f.id}",
    "${hcloud_ssh_key.m.id}",
    "${hcloud_ssh_key.a1.id}",
    "${hcloud_ssh_key.a2.id}",
    "${hcloud_ssh_key.a3.id}",
    "${hcloud_ssh_key.a4.id}",
  ]

  keep_disk   = true
  image       = "${var.hcloud_image}"
  datacenter  = "${var.hcloud_datacenter}"
  server_type = "${var.hcloud_server}"

  # (Optional) Cloud-Init user data to use during server creation
  user_data = "#cloud-config\\nruncmd:\\n- [touch, /root/cloud-init-worked]\\n"

  #  name - (Required) Name of the server to create (must be unique per project and a valid hostname as per RFC 1123).
  #  server_type - (Required) Name of the server type this server should be created with.
  #  image - (Required) Name or ID of the image the server is created from.
  #  location - (Optional) The location name to create the server in.
  #  datacenter - (Optional) The datacenter name to create the server in.
  #  user_data - (Optional) Cloud-Init user data to use during server creation
  #  ssh_keys - (Optional) SSH key IDs or names which should be injected into the server at creation time
  #  keep_disk - (Optional) If true, do not upgrade the disk. This allows downgrading the server type later.
  #  backup_window - (Optional) Enable and configure backups for a server. Time window (UTC) in which the backup will run, choices: 22-02 02-06 06-10 10-14 14-18 18-22
  #  iso - (Optional) Name of an ISO image to mount.
  #  rescue - (Optional) Enable and boot in to the specified rescue system. This enables simple installation of custom operating systems. linux64 linux32 or freebsd64
}

output "ip4" {
  value = "${hcloud_floating_ip.master4.ip_address}"
}

output "ip4" {
  value = "${hcloud_floating_ip.master6.ip_address}"
}
