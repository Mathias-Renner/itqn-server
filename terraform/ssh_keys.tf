resource "hcloud_ssh_key" "default" {
  name       = "ITQN-server"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "hcloud_ssh_key" "a1" {
  name       = "ssh-key Andreas-1"
  public_key = "${file("~/.ssh/keys/id_rsa_a1.pub")}"
}

resource "hcloud_ssh_key" "a2" {
  name       = "ssh-key Andreas-2"
  public_key = "${file("~/.ssh/keys/id_rsa_a2.pub")}"
}

resource "hcloud_ssh_key" "a3" {
  name       = "ssh-key Andreas-3"
  public_key = "${file("~/.ssh/keys/id_rsa_a3.pub")}"
}

resource "hcloud_ssh_key" "a4" {
  name       = "ssh-key Andreas-4"
  public_key = "${file("~/.ssh/keys/id_rsa_a4.pub")}"
}

resource "hcloud_ssh_key" "f" {
  name       = "ssh-key Franz"
  public_key = "${file("~/.ssh/keys/id_rsa_f.pub")}"
}

resource "hcloud_ssh_key" "m" {
  name       = "ssh-key Mathias"
  public_key = "${file("~/.ssh/keys/id_rsa_m.pub")}"
}
